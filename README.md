<div align="center">
  <h1>Fundamentos de Bases de Datos</h1>
</div>
<div align="center">
  <img src="img/logo.png" width="250">
</div>

# Tabla de contenido
- [Bienvenida conceptos básicos](#Bienvenida-conceptos-básicos)
    - [Base de datos](#Base-de-datos)
- [Introducción a las bases de datos relacionales](#Introducción-a-las-bases-de-datos-relacionales)
    - [Historia de las RDB](#Historia-de-las-RDB)
    - [Entidades y atributos](#Entidades-y-atributos)
    - [Entidades de un Blog](#Entidades-de-un-Blog)
    - [Relaciones](#Relaciones)
    - [Diagrama ER](#Diagrama-ER)
    - [Diagrama Físico: tipos de datos y constraints](#Diagrama-Físico-tipos-de-datos-y-constraints)
    - [Diagrama Físico: normalización](#Diagrama-Físico-normalización)
    - [Diagrama Físico: normalizando el blog](#Diagrama-Físico-normalizando-el-blog)
    - [Formas normales en DB relacionales](#Formas-normales-en-DB-relacionales)
- [RDBMS (MySQL) o cómo hacer lo anterior de manera práctica](#RDBMS-MySQL-o-cómo-hacer-lo-anterior-de-manera-práctica)
    - [RDB ¿Qué?](#RDB-¿Qué?)
    - [Instalación local de un RDBMS (Ubuntu)](#Instalación-local-de-un-RDBMS-(Ubuntu))
- [SQL hasta en la sopa](#SQL-hasta-en-la-sopa)
    - [Historia de SQL](#Historia-de-SQL)
    - [DDL](#DDL)
    - [CREATE VIEW y DDL ALTER](#CREATE-VIEW-y-DDL-ALTER)
    - [DDL drop](#DDL-drop)
    - [DML](#DML)
    - [¿Qué tan standard es SQL?](#¿Qué-tan-standard-es-SQL?)
    - [Creando el blod de las tablas independientes](#Creando-el-blog-de-las-tablas-independientes)
    - [Restricciones de integridad referencial](#Restricciones-de-integridad-referencial)
    - [Creando el blod de las tablas dependientes](#Creando-el-blog-de-las-tablas-dependientes)
    - [Creando el blog de tablas transitivas](#Creando-el-blog-de-tablas-transitivas)
- [Consultas a una base de datos](#Consultas-a-una-base-de-datos)
    - [¿Por qué las consultas son tan importantes?](#¿Por-qué-las-consultas-son-tan-importantes?)
    - [Estructura básica de un Query](#Estructura-básica-de-un-Query)
    - [SELECT](#SELECT)
    - [FROM](#FROM)
    - [Utilizando la sentencia FROM](#Utilizando-la-sentencia-FROM)
    - [WHERE](#WHERE)
    - [Utilizando la sentencia WHERE nulo y no nulo](#Utilizando-la-sentencia-WHERE-nulo-y-no-nulo)
    - [GROUP BY](#GROUP-BY)
    - [ORDER BY y HAVING](#ORDER-BY-y-HAVING)
    - [El interminable agujero de conejo (Nested queries)](#El-interminable-agujero-de-conejo-(Nested-queries))
    - [¿Cómo convertir una pregunta en un query SQL?](#¿Cómo-convertir-una-pregunta-en-un-query-SQL?)
    - [Preguntándole a la base de datos](#Preguntándole-a-la-base-de-datos)
- [Introducción a la bases de datos NO relacionales](#Introducción-a-la-bases-de-datos-NO-relacionales)
    - [¿Qué son y cuáles son los tipos de bases de datos no relacionales?](#¿Qué-son-y-cuáles-son-los-tipos-de-bases-de-datos-no-relacionales?)
    - [Servicios administrados y jerarquía de datos](#Servicios-administrados-y-jerarquía-de-datos)


# <a name="Bienvenida-conceptos-básicos">Bienvenida conceptos básicos</a>
## <a name="Base-de-datos">Base de datos</a>

Una **base de datos** es un conjunto de datos pertenecientes a un mismo contexto y almacenados sistemáticamente para su posterior uso.

**Tipos de bases de datos:**

- **Base de datos relacional:** La base de datos relacional (BDR) es un tipo de base de datos (BD) que cumple con el modelo relacional; que se relacionan entre ellos mediante la relación de identificadores.
- **Bases de datos no relacionales:** Como su propio nombre indica, las bases de datos no relacionales son las que, a diferencia de las relacionales, no tienen un identificador que sirva de relación entre un conjunto de datos y otros.

**Servicios:**

- **Auto administrados**: Uno mismo se encargara de administrar las bases de datos, te encargas de la instalacion, actualizacion, mantenimiento, etc.
- **Administrados**: Las bases de datos totalmente administradas automatizan tareas como la configuración y la administración de la alta disponibilidad, la recuperación ante desastres, las copias de seguridad y la replicación de datos entre regiones, lo que le ahorra tiempo y dinero. Servicios que ofrece la nube como Azure.

**URL**
- [Base de datos](https://es.wikipedia.org/wiki/Base_de_datos)
- [Base de datos relacional](https://es.wikipedia.org/wiki/Base_de_datos_relacional)
- [Bases de datos relacionales vs. no relacionales: ¿qué es mejor?](https://aukera.es/blog/bases-de-datos-relacionales-vs-no-relacionales/)

# <a name="Introducción-a-las-bases-de-datos-relacionales">Introducción a las bases de datos relacionales</a>
## <a name="Historia-de-las-RDB">Historia de las RDB</a>

**Introducción**

Los sistemas de administración de bases de datos (DBMS) son programas que permiten a los usuarios interactuar con una base de datos. Les permiten controlar el acceso a una base de datos, escribir datos, ejecutar consultas y realizar otras tareas relacionadas con la administración de bases de datos.

Sin embargo, para realizar cualquiera de estas tareas, los DBMS deben tener algún tipo de modelo subyacente que defina la organización de los datos. El modelo relacional es un enfoque para la organización de datos que se ha utilizado ampliamente en programas de software de bases de datos desde su concepción a fines de la década de 1960.

**Historia del modelo relacional**

Las bases de datos son conjuntos de información, o datos, modelados de forma lógica. Cualquier recopilación de datos es una base de datos, independientemente de cómo o dónde se almacene. Incluso un gabinete de archivos con información sobre nómina es una base de datos, al igual que una pila de formularios de pacientes hospitalizados o la recopilación de información sobre clientes de una empresa repartida en varias ubicaciones. Antes de que almacenar y administrar datos con computadoras se convirtiera en una práctica común, las bases de datos físicas como estas eran lo único con lo que contaban las organizaciones gubernamentales y empresariales que necesitaban almacenar información.

A mediados del siglo XX, los desarrollos en las ciencias de la computación dieron lugar a máquinas con mayor capacidad de procesamiento y almacenamiento, tanto local como externo. Estos avances hicieron que los especialistas en ciencias de la computación comenzaran a reconocer el potencial que tenían estas máquinas para almacenar y administrar cantidades de datos cada vez más grandes.

Sin embargo, no había teorías sobre cómo las computadoras podían organizar datos de manera significativa y lógica. Una cosa es almacenar datos no ordenados en una máquina, pero es mucho más complicado diseñar sistemas que permitan agregar, recuperar, clasificar y administrar esos datos de forma sistemática y práctica. La necesidad de contar con un marco de trabajo lógico para almacenar y organizar datos dio lugar a varias propuestas sobre cómo utilizar las computadoras para administrar datos.

Uno de los primeros modelos de bases de datos fue el modelo jerárquico, en el que los datos se organizan con una estructura de árbol similar a la de los sistemas de archivos modernos. El siguiente ejemplo muestra el aspecto que podría tener el diseño de una parte de una base de datos jerárquica utilizada para categorizar animales:
 
<div align="center">
  <img src="img/Historia.png" width="500">
</div>

El modelo jerárquico se implementó ampliamente en los primeros sistemas de administración de bases de datos, pero resultaba poco flexible. En este modelo, a pesar de que los registros individuales pueden tener varios “elementos secundarios”, cada uno puede tener un solo “elemento primario” en la jerarquía. Es por eso que estas primeras bases de datos jerárquicas se limitaban a representar relaciones “uno a uno” y “uno a varios”. Esta carencia de relaciones “varios a varios” podía provocar problemas al trabajar con puntos de datos que se deseaba asociar a más de un elemento primario.

A fines de los años 60, Edgar F. Codd, un especialista en ciencias de la computación que trabajaba en IBM, diseñó el modelo relacional de administración de bases de datos. El modelo relacional de Codd permitía que los registros individuales se relacionaran con más de una tabla y, de esta manera, posibilitaba las relaciones “varios a varios” entre los puntos de datos además de las relaciones “uno a varios”. Esto proporcionó más flexibilidad que los demás modelos existentes a la hora de diseñar estructuras de base de datos y permitió que los sistemas de gestión de bases de datos relacionales (RDBMS) pudieran satisfacer una gama mucho más amplia de necesidades empresariales.

Codd propuso un lenguaje para la administración de datos relacionales, conocido como Alfa, que influyó en el desarrollo de los lenguajes de bases de datos posteriores. Dos colegas de Codd en IBM, Donald Chamberlin y Raymond Boyce, crearon un lenguaje similar inspirado en Alpha. Lo llamaron SEQUEL, abreviatura de Structured English Query Language (Lenguaje de consulta estructurado en inglés), pero debido a una marca comercial existente, lo abreviaron a SQL (conocido formalmente como* Lenguaje de consulta estructurado*).

Debido a las limitaciones de hardware, las primeras bases de datos relacionales eran prohibitivamente lentas y el uso de la tecnología tardó un tiempo en generalizarse. Pero a mediados de los años ochenta, el modelo relacional de Codd se había implementado en varios productos comerciales de administración de bases de datos, tanto de IBM como de sus competidores. Estos proveedores también siguieron el liderazgo de IBM al desarrollar e implementar sus propios dialectos de SQL. Para 1987, tanto el Instituto Nacional Estadounidense de Estándares (American National Standards Institute) como la Organización Internacional de Normalización (International Organization for Standardization) habían ratificado y publicado normas para SQL, lo que consolidó su estado como el lenguaje aceptado para la administración de RDBMS.

Gracias al uso extendido del modelo relacional en varias industrias, se lo comenzó a reconocer como el modelo estándar para la administración de datos. Incluso con el surgimiento de varias bases de datos NoSQL en los últimos años, las bases de datos relacionales siguen siendo las herramientas predominantes para almacenar y organizar datos.

**Cómo organizan los datos las bases de datos relacionales**

Ahora que tiene una idea general de la historia del modelo relacional, vamos a analizar en detalle cómo organiza los datos.

Los elementos más fundamentales del modelo relacional son las relaciones, que los usuarios y los RDBMS modernos reconocen como tablas. Una relación es un conjunto de tuplas, o filas de una tabla, en el que cada una de ellas comparte un conjunto de atributos o columnas:

<div align="center">
  <img src="img/Historia1.png" width="500">
</div>

Una columna es la estructura de organización más pequeña de una base de datos relacional y representa las distintas facetas que definen los registros en la tabla. De ahí proviene su nombre más formal: atributos. Se puede pensar que cada tupla es como una instancia única de cualquier tipo de personas, objetos, eventos o asociaciones que contenga la tabla.  Estas instancias pueden ser desde empleados de una empresa y ventas de un negocio en línea hasta resultados de pruebas de laboratorio. Por ejemplo, en una tabla que contiene registros de los maestros de una escuela, las tuplas pueden tener atributos como name, subjects, start_date, etc.

Al crear una columna, especificamos un* tipo de datos *que determina el tipo de entradas que se permiten en esa columna. Los sistemas de administración de bases de datos relacionales (RDBMS) suelen implementar sus propios tipos de datos únicos, que pueden no ser directamente intercambiables con tipos de datos similares de otros sistemas. Algunos tipos de datos frecuentes son fechas, cadenas, enteros y booleanos.

En el modelo relacional, cada tabla contiene, por lo menos, una columna que puede utilizarse para identificar de forma única cada fila, lo que se denomina clave primaria. Esto es importante, dado que significa que los usuarios no necesitan saber dónde se almacenan físicamente los datos en una máquina; en su lugar, sus DBMS pueden realizar un seguimiento de cada registro y devolverlo según corresponda. A su vez, significa que los registros no tienen un orden lógico definido y que los usuarios tienen la capacidad de devolver sus datos en cualquier orden y a través de los filtros que deseen.

Si tiene dos tablas que desea asociar, una manera de hacerlo es con una clave externa. Una clave externa es, básicamente, una copia de la clave primaria de una tabla (la tabla “primaria”) insertada en una columna de otra tabla (la “secundaria”). El siguiente ejemplo destaca la relación entre dos tablas: una se utiliza para registrar información sobre los empleados de una empresa y la otra, para realizar un seguimiento de las ventas de la empresa. En este ejemplo, la clave primaria de la tabla EMPLOYEES se utiliza como clave externa de la tabla SALES:

<div align="center">
  <img src="img/Historia2.png" width="500">
</div>

Si intenta agregar un registro a la tabla secundaria y el valor ingresado en la columna de la clave externa no existe en la clave primaria de la tabla primaria, la instrucción de inserción no será válida. Esto ayuda a mantener la integridad del nivel de las relaciones, dado que las filas de ambas tablas siempre se relacionarán correctamente.

Los elementos estructurales del modelo relacional ayudan a mantener los datos almacenados de forma organizada, pero el almacenamiento de datos solo es útil si se pueden recuperar. Para obtener información de un RDBMS, puede emitir una consulta o una solicitud estructurada de un conjunto de información. Como se mencionó anteriormente, la mayoría de las bases de datos relacionales utilizan SQL para administrar y consultar datos. SQL permite filtrar y manipular los resultados de las consultas con una variedad de cláusulas, predicados y expresiones, lo que, a su vez, permite controlar correctamente qué datos se mostrarán en el conjunto de resultados.

**Ventajas y limitaciones de las bases de datos relacionales**

Teniendo en cuenta la estructura organizativa subyacente de las bases de datos relacionales, consideremos algunas de sus ventajas y desventajas.

Hoy en día, tanto SQL como las bases de datos que lo implementan se desvían del modelo relacional de Codd de diversas maneras. Por ejemplo, el modelo de Codd determina que cada fila de una tabla debe ser única, mientras que, por cuestiones de practicidad, la mayoría de las bases de datos relacionales modernas permiten la duplicación de filas. Algunas personas no consideran que las bases de datos SQL sean verdaderas bases de datos relacionales a menos que cumplan con las especificaciones de Codd del modelo relacional. Sin embargo, en términos prácticos, es probable que cualquier DBMS que utilice SQL y, por lo menos, se adhiera en cierta medida al modelo relacional se denomine “sistema de administración de bases de datos relacionales”.

A pesar de que las bases de datos relacionales ganaron popularidad rápidamente, algunas de las deficiencias del modelo relacional comenzaron a hacerse evidentes a medida que los datos se volvieron más valiosos y las empresas comenzaron a almacenar más de ellos. Entre otras cosas, puede ser difícil escalar una base de datos relacional de forma horizontal.  Los términos escalado horizontal o escala horizontal se refieren a la práctica de agregar más máquinas a una pila existente para extender la carga y permitir un mayor tráfico y un procesamiento más rápido. Se suele contrastar con el escalado vertical, que implica la actualización del hardware de un servidor existente, generalmente, añadiendo más RAM o CPU.

El motivo por el cual es difícil escalar una base de datos relacional de forma horizontal está relacionado con el hecho de que el modelo relacional se diseñó para garantizar coherencia, lo que significa que los clientes que realizan consultas en la misma base de datos siempre obtendrán los mismos datos. Al querer escalar una base de datos relacional de forma horizontal en varias máquinas, resulta difícil asegurar la coherencia, dado que los clientes pueden escribir datos en un nodo pero no en otros.  Es probable que haya un retraso entre la escritura inicial y el momento en que se actualicen los demás nodos para reflejar los cambios, lo que daría lugar a inconsistencias entre ellos.

Otra limitación que presentan los RDBMS es que el modelo relacional se diseñó para administrar datos estructurados o alineados con un tipo de datos predeterminado o, por lo menos, organizado de una manera predeterminada para que se puedan ordenar o buscar de forma más sencilla. Sin embargo, con la difusión de los equipos informáticos personales y el auge de Internet a principios de la década de 1990, los datos no estructurados, como los mensajes de correo electrónico, las fotos, los videos, etc., se volvieron más comunes.

Nada de esto significa que las bases de datos relacionales no sean útiles. Al contrario, después de más de 40 años, el modelo relacional sigue siendo el marco dominante para la administración de datos. Su prevalencia y longevidad indican que las bases de datos relacionales son una tecnología madura, lo que constituye una de sus principales ventajas. Hay muchas aplicaciones diseñadas para trabajar con el modelo relacional, así como muchos administradores de bases de datos profesionales expertos en bases de datos relacionales. También hay una amplia variedad de recursos disponibles en formato impreso y digital para quienes desean comenzar a utilizar bases de datos relacionales.

Otra ventaja de las bases de datos relacionales es que casi todos los RDBMS admiten transacciones. Las transacciones constan de una o más instrucciones SQL individuales que se ejecutan en secuencia como una unidad de trabajo única. Las transacciones tienen un enfoque de todo o nada, lo que significa que todas las instrucciones SQL de una transacción deben ser válidas; de lo contrario, falla en su totalidad. Esto es muy útil para garantizar la integridad de los datos al realizar cambios en varias filas o tablas.

Por último, las bases de datos relacionales son sumamente flexibles. Se han utilizado para crear una amplia variedad de aplicaciones distintas y siguen funcionando de forma eficiente incluso con grandes cantidades de datos. SQL también es sumamente potente y permite agregar y cambiar datos sobre la marcha, así como alterar la estructura de los esquemas y las tablas de las bases de datos sin afectar a los datos existentes.

**URL**

- [Historia del modelo relacional](https://www.digitalocean.com/community/tutorials/understanding-relational-databases-es)
- [12 reglas de Edgar Codd](https://www.w3resource.com/sql/sql-basic/codd-12-rule-relation.php)

## <a name="Entidades-y-atributos">Entidades y atributos</a>

**Entidad:**

Es algo similar a un objeto (programación orientada a objetos) y representa algo en el mundo real, incluso algo abstracto. Tienen atributos que son las cosas que los hacen ser una entidad y por convención se ponen en plural.
en en plural. (Se encierra en un recuadro)


**Atributos:**

Características o propiedades que describen a la entidad (se encierra en un ovalo)

**Atributo multivaluado:**

Tiene un conjunto de valores para una entidad(pueden tener mas de un atributo del mismo tipo)(Se encierra en un doble ovalo).


**Atributo compuesto:**

Se puede dividir en subpartes (es decir, en otros atributos) (de él salen otros atributos)


**Atributo especial o derivados:**

Su valor se puede obtener a partir de valores de otros atributos (se encierra en un ovalo punteado)

**Atributos llave:**

Característica o propiedad única de la entidad, lo que lo diferencia de los demás

- Atributos llave naturales: Son parte de la entidad y no se pueden separar.
- Atributo llave artificial: No son partes de la entidad y se les asignan de manera arbitraria para facilitar el manejo de la información.


**Entidades fuertes:**

Son entidades que pueden sobrevivir por si solo, tip: al leer la tabla sabes que son las atributos (se encierra en un recuadro).

**Entidades Débiles:**

No puede existir sin una entidad fuerte, tip: al leer la tabla no puedes saber que son las atributos (se encierra en un recuadro doble).

Las identidades son débiles por dos motivos:
- Por identidad: Se diferencia entre sí por la clave de su entidad fuerte. Para que dejen de ser débiles por identidad, se les puede asignar una clave propia.
- Por existencia: Son las entidades que han dejado de ser débiles por identidad al asignárseles una clave, pero son por existencia, ya que siguen dependiendo de la entidad fuerte para existir.

**Estilo de la notación de Chen**

La notación Chen es una notación gráfica para modelos de relación de entidades.

<div align="center">
  <img src="img/Entidades_atributos.png" width="500">
</div>

**Ejemplo**:

<div align="center">
  <img src="img/Entidades_atributosEjemplo.png" width="700">
</div>

## <a name="Entidades-de-un-Blog">Entidades de un Blog</a>

Nuestro proyecto será un manejador de Blogpost. Es un contexto familiar y nos representará retos muy interesantes.

- Primer paso: Identificar las entidades
- Segundo paso: Pensar en los atributos

## Entidades del Blog diagrama ER

<div align="center">
  <img src="img/img1.png" width="500">
</div>

## Atributos de la entidad de posts

<div align="center">
  <img src="img/img2.png" width="500">
</div>

## Atributos de la entidad de usuarios

<div align="center">
  <img src="img/img3.png" width="500">
</div>


## <a name="Relaciones">Relaciones</a>

Las relaciones nos permiten ligar o unir nuestras diferentes entidades y se representan con rombos. Por convención se definen a través de verbos.

<div align="center">
  <img src="img/img4.png" width="500">
</div>

Las relaciones tienen una propiedad llamada cardinalidad y tiene que ver con números. Cuántos de un lado pertenecen a cuántos del otro lado:

- Cardinalidad: 1 a 1
- Cardinalidad: 0 a 1
- Cardinalidad: 1 a N
- Cardinalidad: 0 a N


> Para sacar la cardinalidad entre las dos entidades lo que se hace es tomar el numero mayor de ambos lados

#### Cardinalidad: 1 a 1
Es una relacion de **uno a uno**, por ejemplo, una persona solo puede tener un dato de contacto y un dato de contacto solo puede pertenecer a una persona

<div align="center">
  <img src="img/img5.png" width="500">
</div>

#### Cardinalidad: 0 a 1

Es una relacion de **cero a uno** o de **uno opcional a uno**, cuando uno de los elementos no necesariamente existe. Por ejemplo (Puede que el usuario tenga o no una sesion abierta).

<div align="center">
  <img src="img/img6.png" width="500">
</div>

#### Cardinalidad: 1 a N

Es una relacion de **1 a n** o **1 a muchos**. cuando una entidad se relaciona con multiples entidades. Ejemplo una persona puede tener muchos automoviles sin embargo cada automovil solo debe de tener un dueño.

<div align="center">
  <img src="img/img7.png" width="500">
</div>

#### Cardinalidad: 0 a N

Es una relacion de **0 a muchos** o **uno opcional a muchos**. Cuando alguna de las entidades puede estar vacía.  Ejemplo las habitaciones de hospital tienen pacientes (puede darse el caso de que una habitación este vacía).

<div align="center">
  <img src="img/img8.png" width="500">
</div>


- habitacion 1 : 1 paciente
- habitacion 1 : n pacientes
- habitacion 1 : 0 pacientes

El resultado es habitacion 1 : n pacientes. Sin embargo dado que existe el caso en que puede tener 0 pacientes algunos autores lo resaltan como 0:N o 0:1 a mí me sirve más verlo como que son relaciones 1:N


#### Cardinalidad: N a N

Es una relacion de **muchos a muchos**. por ejemplo un alumno puede tomar varias clases como matematicas, español o Base de Datos por ejemplo, tambien una clase puede tener barios alumnos.

<div align="center">
  <img src="img/img9.png" width="500">
</div>


## <a name="Diagrama-ER">Diagrama ER</a>

Un modelo entidad-relación es una herramienta para el modelo de datos, la cual facilita la representación de entidades de una base de datos.​ Fue definido por Peter Chen en 1976.

Un diagrama es como un mapa y nos ayuda a entender cuáles son las entidades con las que vamos a trabajar, cuáles son sus relaciones y qué papel van a jugar en las aplicaciones de la base de datos.

<div align="center">
  <img src="img/img10.png" width="500">
</div>


- [Cardinalities](https://en.wikipedia.org/wiki/Entity%E2%80%93relationship_model#Cardinalities)
- [diagrams](https://app.diagrams.net/)


## <a name="Diagrama-Físico-tipos-de-datos-y-constraints">Diagrama Físico: tipos de datos y constraints</a>
Para llevar a la práctica un diagrama debemos ir más allá y darle detalle con parámetros como:

#### Tipos de dato:

<div align="center">
  <img src="img/img11.png" width="500">
</div>

- Tipos de datos de texto:
    - **CHAR(n)** Permite almacenar caracteres y cadenas de caracteres.
    Este tipo de dato reserva un espacio de memoria del número de caracteres que va a ser ocupado.
    - **VARCHAR(n)** Al igual que char, este reserva espacio en la memoria. Su diferencia radica en que este reserva un mínimo espacio de memoria, y a partir de esta va creciendo o encogiéndose, es eficiente cuando desconocés cual será el tamaño de tu cadena de texto (Su limite es de 255 caracteres).
    - **TEXT** Su función es guardar cadenas de texto que sean muuuuuy grandes.
- Tipos de datos numéricos:
    - **INTEGER** Número que no tiene punto decimal, se usa para declarar un tipo de dato entero que puede ser usado para hacer operaciones.?Al usar este tipo de dato nuestra base de datos sabrá que estamos hablando de número y no solo de un simple carácter.
    - **BIGINT** Es un subtipo de integer, nos sirve para declarar números muy grandes.
    - **SMALLINT** Subtipo de integer, para declarar números muy pequeños (99 o menos).
    - **DECIMAL(n, s)** **NUMERIC(n, s)** Tiene dos parámetros (n y s, en este ejemplo). La primera entrada es para números enteros, y la segunda entrada es para números decimales.?Nos sirve para hacer operaciones mas precisas.
- Tipos de datos de fecha y hora:
    - Esta clase de tipos de datos es muy peculiar ya que nos ayuda internamente a tener una bitácora de nuestra base de datos.
    - **DATE** Solo contiene la fecha (año, mes y día).
    - **TIME** Solo contiene la hora.
    - **DATETIME** Es una mezcla de los dos primeros, contiene fecha y hora.
    - **TIMESTAMP** Es el número de segundos que ha transcurrido desde que tu archivo fue creado.?En otras palabras, podría decirse que es un medidor de tiempo.
- Tipos de datos lógicos:
    - **BOOLEAN** Este solo puede tener dos valores

#### Constraints (Restricciones)

<div align="center">
  <img src="img/img12.png" width="500">
</div>

Los contraints o restricciones son los tipos de reglas que vas a permitir que tenga tu base de datos.
Para ello usamos las siguientes:

- Constraints (Restricciones):
    - **NOT NULL** Se asegura que tu columna no tenga valores nulos.
    - **UNIQUE** Asegura que cada valor en tu columna no se repita.
    - **PRIMARY KEY** Es una etiqueta muy importante, es una combinación entre not null y unique.?Nos permite hacer relaciones entre distintas entidades.
    - **FOREIGN KEY** Llave foránea, es el otro lado de una primary key, cuando queremos juntar dos tablas y decir que estan relacionadas entre si, lo que va a suceder es la primary key de una de las tablas se añadirá como foreign key de la otra.
    - **CHECK** Algunas bases de datos removieron este tipo de contraints, pero las que lo conservan son muy potentes.?Tiene la función de permitir que añadamos las reglas que queramos a nuestra base de datos.
    - **INDEX** Se crea por columna, su función es hacer búsquedas con mayor rapidez.?Su única desventaja es que suele volverse lenta cada vez que se añaden nuevos registros.

## <a name="Diagrama-Físico-normalización">Diagrama Físico: normalización</a>

La normalización como su nombre lo indica nos ayuda a dejar todo de una forma normal. Esto obedece a las 12 reglas de Codd y nos permiten separar componentes en la base de datos:

- **Primera forma normal** (1FN): Atributos atómicos (Sin campos repetidos)
- **Segunda forma normal** (2FN): Cumple 1FN y cada campo de la tabla debe depender de una clave única.
- **Tercera forma normal** (3FN): Cumple 1FN y 2FN y los campos que NO son clave, NO deben tener dependencias.
- **Cuarta forma normal** (4FN): Cumple 1FN, 2FN, 3FN y los campos multivaluados se identifican por una clave única.

<div align="center">
  <img src="img/normalizacion.png" width="1000">
</div>
<div align="center">
  <img src="img/normalizacion1.png" width="1000">
</div>
<div align="center">
  <img src="img/normalizacion2.png" width="1000">
</div>
<div align="center">
  <img src="img/normalizacion3.png" width="1000">
</div>



## <a name="Diagrama-Físico-normalizando-el-blog">Diagrama Físico: normalizando el blog</a>

**Anotación importante**

- Regla general: cuanto tienes una cardinalidad 1 a 1 no importa a que tabla le coloques la referencia (llave foranea) es indistinto.
- Cuando tienes una cardinalidad 1:N es muy importante que la tabla donde tienes la terminación mucho(N), en esa tabla colocaras la llave foranea que ara referencia a la tabla que tiene la terminación de uno(1).**Ejemplo**: Lo estaremos relacionando añadiendo la llave primaria en la entidad **usuarios** y como llave foranea en la otra entidad en este caso es **comentarios**.


Ejemplo:

<div align="center">
  <img src="img/Diagrama.png" width="500">
</div>

**Caso especial de la Cardinalidad: N a N**

> Sigiendo las normas de las reglas general aque table le ponemos la llave foranea si es una cardinalidad de muchos(N) de los dos lados(O las dos tablas tiene una cardinalidad de mucho) y no tenemos uno que tenga la cardinalidad de (1). Esto es un caso especial

<div align="center">
  <img src="img/Diagrama1.png" width="500">
</div>


> Lo que se hace en estos casos  es romper la relacion de N a N o muchos a muchos poniendo una tabla intermedia que se llama tabla pibote y quedaria de la siguiente forma

<div align="center">
  <img src="img/Diagrama2.png" width="500">
</div>

> Investigar mas sobre este tema porque se supone que esta esta mal relacionada al parecer esta invertido la relación que tiene la tabla pivote con las otras dos tablas porque La relación debería ir en sentido contrario, en ambas partes



## <a name="Formas-normales-en-DB-relacionales">Formas normales en DB relacionales</a>


La normalización en las bases de datos relacionales es uno de esos temas que, por un lado es sumamente importante y por el otro suena algo esotérico. Vamos a tratar de entender las formas normales (FN) de una manera simple para que puedas aplicarlas en tus proyectos profesionales.

**Primera Forma Normal (1FN)**

Esta FN nos ayuda a eliminar los valores repetidos y no atómicos dentro de una base de datos.

Formalmente, una tabla está en primera forma normal si:

- Todos los atributos son atómicos. Un atributo es atómico si los elementos del dominio son simples e indivisibles.
- No debe existir variación en el número de columnas.
- Los campos no clave deben identificarse por la clave (dependencia funcional).
- Debe existir una independencia del orden tanto de las filas como de las columnas; es decir, si los datos cambian de orden no deben cambiar sus significados.


Se traduce básicamente a que si tenemos campos compuestos como por ejemplo “nombre_completo” que en realidad contiene varios datos distintos, en este caso podría ser “nombre”, “apellido_paterno”, “apellido_materno”, etc.

También debemos asegurarnos que las columnas son las mismas para todos los registros, que no haya registros con columnas de más o de menos.

Todos los campos que no se consideran clave deben depender de manera única por el o los campos que si son clave.

Los campos deben ser tales que si reordenamos los registros o reordenamos las columnas, cada dato no pierda el significado.

**Segunda Forma Normal (2FN)**

Esta FN nos ayuda a diferenciar los datos en diversas entidades.

Formalmente, una tabla está en segunda forma normal si:

- Está en 1FN
- Sí los atributos que no forman parte de ninguna clave dependen de forma completa de la clave principal. Es decir, que no existen dependencias parciales.
- Todos los atributos que no son clave principal deben depender únicamente de la clave principal.


Lo anterior quiere decir que sí tenemos datos que pertenecen a diversas entidades, cada entidad debe tener un campo clave separado. Por ejemplo:



<div align="center">
  <img src="img/Formas.jpg" width="500">
</div>

En la tabla anterior tenemos por lo menos dos entidades que debemos separar para que cada uno dependa de manera única de su campo llave o ID. En este caso las entidades son alumnos por un lado y materias por el otro. En el ejemplo anterior, quedaría de la siguiente manera:


<div align="center">
  <img src="img/Formas1.jpg" width="500">
</div>


**Tercera Forma Normal (3FN)**

Esta FN nos ayuda a separar conceptualmente las entidades que no son dependientes.

Formalmente, una tabla está en tercera forma normal si:

Se encuentra en 2FN
No existe ninguna dependencia funcional transitiva en los atributos que no son clave

Esta FN se traduce en que aquellos datos que no pertenecen a la entidad deben tener una independencia de las demás y debe tener un campo clave propio. Continuando con el ejemplo anterior, al aplicar la 3FN separamos la tabla alumnos ya que contiene datos de los cursos en ella quedando de la siguiente manera.

<div align="center">
  <img src="img/Formas3.jpg" width="500">
</div>

<div align="center">
  <img src="img/Formas4.jpg" width="500">
</div>


**Cuarta Forma Normal (4FN)**

Esta FN nos trata de atomizar los datos multivaluados de manera que no tengamos datos repetidos entre rows.

Formalmente, una tabla está en cuarta forma normal si:

- Se encuentra en 3FN
- Los campos multivaluados se identifican por una clave única

Esta FN trata de eliminar registros duplicados en una entidad, es decir que cada registro tenga un contenido único y de necesitar repetir la data en los resultados se realiza a través de claves foráneas.

Aplicado al ejemplo anterior la tabla materia se independiza y se relaciona con el alumno a través de una tabla transitiva o pivote, de tal manera que si cambiamos el nombre de la materia solamente hay que cambiarla una vez y se propagara a cualquier referencia que haya de ella.



<div align="center">
  <img src="img/Formas6.jpg" width="500">
</div>

<div align="center">
  <img src="img/Formas7.jpg" width="500">
</div>

De esta manera, aunque parezca que la información se multiplicó, en realidad la descompusimos o normalizamos de manera que a un sistema le sea fácil de reconocer y mantener la consistencia de los datos.

Algunos autores precisan una 5FN que hace referencia a que después de realizar esta normalización a través de uniones (JOIN) permita regresar a la data original de la cual partió.


# <a name="RDBMS-MySQL-o-cómo-hacer-lo-anterior-de-manera-práctica">RDBMS (MySQL) o cómo hacer lo anterior de manera práctica</a>


## <a name="RDB-¿Qué?">RDB ¿Qué?</a>

- RDB (relational database)
- RDBMS (Relational DataBase Magement System) Sistema Manejador de Bases de datos relacionales.Un sistema que permite crear, editar y administrar una base de datos relacional. En su gran mayoría usan el Lenguaje de Consultas Estructuradas (SQL).

La diferencia entre ambos es que las BBDD son un conjunto de datos pertenecientes ( o al menos en teoría) a un mismo tipo de contexto, que guarda los datos de forma persistente para un posterior uso, y el Sistema de gestión de BBDD o sistema manejador, es el que nos permite acceder a ella, es un software, herramienta que sirve de conexión entre las BBDD y el usuario (nos presenta una interfaz para poder gestionarla, manejarla).

RDBMS

- MySQL
- PostgreSQL
- Etc

Todas toman un lenguaje base, pero cada uno lo apropia, imponiéndole diferentes reglas y características.


## <a name="Instalación-local-de-un-RDBMS-(Ubuntu)">Instalación local de un RDBMS (Ubuntu)</a>

Visita la dirección de descarga de la versión de comunidad de MySql
https://dev.mysql.com/downloads/mysql/5.7.html#downloads

Dirígete a la sección de selección de descargas y selecciona tu distribución de Linux. En nuestro caso Ubuntu y selecciona posteriormente la versión que estás utilizando actualmente, en nuestro caso 18.04 de 64 bits.


<div align="center">
  <img src="img/Ubuntu.png" width="500">
</div>


Más abajo encontrarás las diferentes opciones de descarga existen diversos paquetes dependiendo tus necesidades. En el caso del ejemplo usaremos la versión deb bundle. Da click en el botón Download seleccionado.

<div align="center">
  <img src="img/Ubuntu1.png" width="500">
</div>


En la siguiente pantalla nos piden que nos registremos o iniciemos sesión, pero ya que solo queremos la descarga daremos click en el link que se encuentre en la parte de abajo.

<div align="center">
  <img src="img/Ubuntu2.png" width="500">
</div>

Espera a que la descarga concluya.
Al terminar abre el archivo .tar con el desempaquetador de tu preferencia.

<div align="center">
  <img src="img/Ubuntu3.png" width="500">
</div>

Extrae el contenido en la carpeta de tu preferencia.


<div align="center">
  <img src="img/Ubuntu4.png" width="500">
</div>


Selecciona el archivo de servidor de comunidad y ábrelo con tu manejador de paquetes instalado.

<div align="center">
  <img src="img/Ubuntu5.png" width="500">
</div>
<div align="center">
  <img src="img/Ubuntu6.png" width="500">
</div>

Da click en instalar.
Finalmente puedes ir a la consola o terminal de Ubuntu y escribir el siguiente comando.
sudo mysql


<div align="center">
  <img src="img/Ubuntu7.png" width="500">
</div>

A continuación deberá aparecer una ventana con el prompt de mysql donde ya puedes comenzar a ejecutar los comandos de las lecciones.

<div align="center">
  <img src="img/Ubunto8.png" width="500">
</div>


Nota: recuerda estar seguro que las dependencias para el paquete se cumplen para instalar.
Nota: muchas veces las distribuciones ya cuentan con paquetes en su repositorio, en ese caso también puedes ejecutar el comando:
sudo apt-get install mysql-server



# <a name="SQL-hasta-en-la-sopa">SQL hasta en la sopa</a>

## <a name="Historia-de-SQL">Historia de SQL</a>

SQL significa Structured Query Language y tiene una estructura clara y fija. Su objetivo es hacer un solo lenguaje para consultar cualquier manejador de bases de datos volviéndose un gran estándar.

Ahora existe el NOSQL o Not Only Structured Query Language que significa que no sólo se utiliza SQL en las bases de datos no relacionales.


## Los comandos SQL se dividen en cuatro subgrupos, DDL, DML, DCL y TCL.

<div align="center">
  <img src="img/comandos.jpg" width="500">
</div>

- [DDL, DML, DCL y TCL.](https://www.w3schools.in/mysql/ddl-dml-dcl/)


## <a name="DDL">DDL</a>

Data Definition Language que nos ayuda a crear la estructura de una base de datos.

Existen 3 grandes comandos:

- Create: Nos ayuda a crear bases de datos, tablas, vistas, índices, etc.
- Alter: Ayuda a alterar o modificar entidades.
- Drop: Nos ayuda a borrar. Hay que tener cuidado al utilizarlo.

3 objetos que manipularemos con el lenguaje DDL:

- Database o bases de datos
- Table o tablas. Son la traducción a SQL de las entidades
- View o vistas: Se ofrece la proyección de los datos de la base de datos de forma entendible.


### CREATE

**Dos formas de crear una base de datos**

```sql
-- ##################################################################### --
-- Primera forma
-- Crea la base de datos y si ya existe manda un error
CREATE DATABASE chanim;

-- ##################################################################### --

-- Segunda forma
-- Crea la base de datos si aun no existe y si ya existe manda un Warning
CREATE DATABASE IF NOT EXISTS chanim;

-- ##################################################################### --
```

**Entramos a la base de datos**

```sql
USE chanim;
```

**Dos formas de crear una tabla o entidad**

```sql
-- ##################################################################### --

-- Primera forma
-- Crea la tabla y si ya existe manda un error
CREATE TABLE usuario(

);

-- ##################################################################### --

-- Segunda forma
-- Crea la tabla si aun no existe y si ya existe manda un Warning
CREATE TABLE IF NOT EXISTS usuario(

);

-- ##################################################################### --
```

**Ejemplo:**

```sql
--AUTO_INCREMENT se agrega cuando se establece una llave primaria
CREATE TABLE IF NOT EXISTS usuario (
  -- Atributo \\// Valores/Constraints
  id INT NOT NULL,
  nombre VARCHAR(255) NOT NULL,
  apellido VARCHAR(255) NOT NULL,
  cuidad VARCHAR(255) NOT NULL
  -- Le indicamos directamente que el atributo id sera nuestra clave primaria
  -- PRIMARY KEY (id)
);
```

**Algunas notas**

```sql
-- Caracter para no tener algun error al manejar hacentos o
-- eñes o algun caracter especial del español
utf8

-- visualizamos el Warning
SHOW WARNINGS;
```

## <a name="CREATE-VIEW-y-DDL-ALTER">CREATE VIEW y DDL ALTER</a>

Las vistas en MySQL (VIEWS) son tablas virtuales. Es decir, tablas que no guardan ningún dato propiamente dentro de ellas. Solo muestran los datos que están almacenados en otras tablas (que sí son reales).

Siendo así, crear vistas en MySQL significa mostrar información de una fuente de origen sin necesidad de mostrar ni exponer a la fuente en sí. En lenguaje técnico, las VIEWS no son más que SELECT Queries y los convierten en algo que podamos consultar frecuentemente.

### VIEW

**Crear una vista**

```sql
-- ##################################################################### --

-- Nombre que le queremos asignar a nuestra view, que usualmente
-- por convención, empieza por “v”, podríamos considerarlo una buena práctica)
CREATE VIEW v_nombreVista AS
-- Sentencia para seleccionar los atributos que queremos extraer de las tablas
-- correspondientes
SELECT atributo, atributo1
-- Indica desde que tabla de la BBDD se extraen los datos
FROM tabla
-- Indicamos una condición, que en este caso sería que solo queremos que nos
-- muestre los datos segun la condicion indicada
WHERE atributo2 = "valor";

-- ##################################################################### --

-- Ejemplo: 0
CREATE VIEW v_brasilCustomers AS
SELECT customers_name, contact_name FROM clientes WHERE country = “Brasil”;
-- Consultamos la vista creada
SELECT * FROM v_brasilCustomers;

-- ##################################################################### --

-- Ejemplo: 1
-- Crear una vista seleccionando todo una tabla
CREATE VIEW v_holaMundo AS SELECT * FROM producto;
-- Consultamos la vista creada
SELECT * FROM v_holaMundo;

-- ##################################################################### --
```

### ALTER

**Nos permite modificar**

```sql
-- ##################################################################### --

-- Agregar una atributo a una tabla lo agrega hasta el final
ALTER TABLE tabla ADD atributo valor valor1;
-- Ejemplo:
ALTER TABLE cliente ADD nombre VARCHAR(30) NOT NULL;

-- ##################################################################### --

-- Alterarla o cambiar los valores de un atributo despues de que fue creada
ALTER TABLE tabla MODIFY COLUMN atributo nuevoValor nuevoValor1 nuevoValor2;
-- Ejemplo:
ALTER TABLE cliente MODIFY COLUMN nombre VARCHAR(60) NOT NULL;

-- ##################################################################### --

-- Eliminar un atributo de una tabla
ALTER TABLE tabla DROP COLUMN atributo;
-- Ejemplo:
ALTER TABLE cliente DROP COLUMN nombre;

-- ##################################################################### --
```

**Clave primaria**

```sql
-- ##################################################################### --

-- Para agregar una clave primaria a un atributo de un tabla existente:
-- Para estableserlo y que no nos traiga conflictos debemos de
-- Asignar primero como clave primaria al atributo correspondiente
ALTER TABLE tabla ADD PRIMARY KEY (atributo);
-- Ejemplo
ALTER TABLE usuario ADD PRIMARY KEY (id);

-- ##################################################################### --

-- Luego debemos de modificarlo sus valores indicando que sera auto incrementable
ALTER TABLE tabla MODIFY COLUMN atributo valor valor1 valor2 AUTO_INCREMENT;
-- Ejemplo
ALTER TABLE usuario MODIFY COLUMN id INT NOT NULL AUTO_INCREMENT;

-- ##################################################################### --

-- Para eliminar una clave primaria: primero debemos de eliminar
-- el valor AUTO_INCREMENT del atributo que tiene por valor la llave primaria
-- lo eliminasmo modificando y dejando todos lo valores menos el AUTO_INCREMENT
ALTER TABLE tabla MODIFY COLUMN atributo valor valor valor;
-- Ejemplo
ALTER TABLE usuario MODIFY COLUMN id INT NOT NULL;

-- ##################################################################### --

-- Ahora si ya podemos eliminar la llave primaria o PRIMARY KEY
ALTER TABLE tabla DROP PRIMARY KEY;
-- Ejemplo
ALTER TABLE usuario DROP PRIMARY KEY;

-- ##################################################################### --
```

## <a name="DDL-drop">DDL drop</a>

Está puede ser la sentencia ¡más peligrosa! (????), sobre todo cuando somos principiantes. Básicamente borra o desaparece de nuestra base de datos.

```sql
-- ##################################################################### --

-- Borrar una base de datos
DROP DATABASE nombreDeLaBaseDeDatos;
-- Ejemplo:
DROP DATABASE chanim;

-- ##################################################################### --

-- Borrar una tabla
DROP TABLE nombreDeLaTabla;
-- Ejemplo:
DROP TABLE cliente;

-- ##################################################################### --

-- Borrar un vista
DROP VIEW v_nombreDeLaVista;
-- Ejemplo
DROP VIEW v_Chanim;

-- ##################################################################### --
```


## <a name="DML">DML</a>

DML trata del contenido de la base de datos. Son las siglas de Data Manipulation Language y sus comandos son:

- Insert: Inserta o agrega nuevos registros a la tabla.
- Update: Actualiza o modifica los datos que ya existen.
- Delete: Esta sentencia es riesgosa porque puede borrar el contenido de una tabla.
- Select: Trae información de la base de datos.

**Insert**

```sql
-- ##################################################################### --

-- Insertamos o agregamos nuevos registros a la tabla.
INSERT INTO nombreDeLaTabla (atributo, atributo1, atributo2)
VALUES ('valor', 'valor1', 'valor2');
-- Ejemplo:
INSERT INTO cliente (nombre, apellido, edad) VALUES ('Luis', 'Morales', 12);

-- ##################################################################### --
```

**Update**

```sql
-- ##################################################################### --

-- Actualizamos o modificamos los datos que ya existen.

-- ##################################################################### --

-- ¡IMPORTANTE!: Evitar a toda costa este tipo de script
-- Porque seran afectados todas las columnas de la tabla existentes
UPDATE cliente SET nombre = 'luis';

-- ##################################################################### --

-- Forma correcta de utilixar UPDATE
UPDATE nombreDeLaTabla SET atributo = 'valor', atributo1 = 'valor1' WHERE atributo2 = 'valor2';
-- Ejemplo: Donde sera modificado solo un registro
-- Ejemplo: Solo sera modificado que tenga el atributo id con el valor 1
UPDATE clinte SET nombre = 'Luis', apellido = 'Morales' WHERE id = 1;

-- ##################################################################### --
```

**Delete**

```sql
-- ##################################################################### --

-- Solo elimina el contenido menos la estructura

-- ##################################################################### --

-- ¡IMPORTANTE!: Evitar a toda costa este tipo de script
-- Borra todo el contenido de la tabla
DELETE FROM cliente

-- ##################################################################### --

-- Ejemplo: Donde sera borrado solo un registro
DELETE FROM nombreDeLaTabla WHERE atributo = 'valor';
-- Ejemplo: Solo sera borrado la columna que tenga el atributo con valor a 1
DELETE FROM cliente WHERE id = 1;

-- ##################################################################### --

-- Ejemplo: Seran borrados las columnas que tengan el atributo nombre con el valor Luis
DELETE FROM clinte WHERE nombre = 'Luis';

-- ##################################################################### --
```

**Select**

```sql
-- ##################################################################### --

-- Trae información de la base de datos.
SELECT atributo, atributo1 FROM nombreDeLaTabla;
-- Ejemplo:
SELECT id, nombre FROM cliente;

-- ##################################################################### --
```


## <a name="¿Qué-tan-standard-es-SQL?">¿Qué tan standard es SQL?</a>

La utilidad más grande de SQL fue unificar la forma en la que pensamos y hacemos preguntas a un repositorio de datos. Ahora que nacen nuevas bases de datos igualmente siguen tomando elementos de SQL.

## <a name="Creando-el-blog-de-las-tablas-independientes">Creando el blod de las tablas independientes</a>

- Una buena práctica es comenzar creando las entidades que no tienen una llave foránea.
- Generalmente en los nombres de bases de datos se evita usar eñes o acentos para evitar problemas en los manejadores de las bases de datos.


<div align="center">
  <img src="img/independientes.png" width="500">
</div>

```sql
CREATE DATABASE blog;
USE blog;
```
```sql
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `nickname` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

CREATE TABLE `etiquetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_etiqueta` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
```

## <a name="Restricciones-de-integridad-referencial">Restricciones de integridad referencial</a>

Las relaciones existentes entre distintas tablas de una base de datos MySQL que utilizan el motor de almacenamiento InnoDB pueden estar especificadas en forma de restricciones de clave externa (“Foreign Key Constraints”), de manera que la propia base de datos impida que se realicen operaciones que provocarían inconsistencias.

El comportamiento por defecto de una restricción de clave externa es impedir un cambio en la base de datos como consecuencia de una sentencia DELETE o UPDATE, si esta trajese como consecuencia un fallo de la integridad referencial.

Veremos primero en resumen las diferentes restricciones de integridad referencial, haciendo uso de algunas imágenes para el caso de ON DELETE.

Las imágenes consideran dos tablas personas y ciudades relacionadas mediante la columna ciudad_id:

<div align="center">
  <img src="img/restricciones.png" width="500">
</div>

**RESTRICT**

RESTRICT: Es el comportamiento por defecto, que impide realizar modificaciones que atentan contra la integridad referencial.

En la imagen vemos el resultado de esta consulta:

```sql
DELETE FROM ciudades WHERE ciudad_id=4;
```
Vemos que el registro se puede borrar porque no existe registro relacionado en la tabla personas.

<div align="center">
  <img src="img/RESTRICT.png" width="500">
</div>

En cambio esta consulta:
```sql
DELETE FROM ciudades WHERE ciudad_id=1;
```

Arrojaría un mensaje de error:

> Cannot delete or update a parent row: a foreign key constraint fails (db.personas, CONSTRAINT personas_ibfk_1 FOREIGN KEY (ciudad_id) REFERENCES ciudades (ciudad_id))

Porque el DELETE viola la restricción. Si la fila 1 de ciudades se borrase, las filas 1 y 4 de personas quedarían huérfanas, o sea, sin relación en la tabla ciudades.

**CASCADE**

CASCADE: Borra los registros de la tabla dependiente cuando se borra el registro de la tabla principal (en una sentencia DELETE), o actualiza el valor de la clave secundaria cuando se actualiza el valor de la clave referenciada (en una sentencia UPDATE).

En la imagen vemos el resultado de esta consulta:

```sql
DELETE FROM ciudades WHERE ciudad_id=1;
```
Aquí se borrarán en cascada CASCADE todos los registros de personas que tengan ciudad_id igual a 1, y como es evidente, se borrará en ciudades la ciudad con id 1.


<div align="center">
  <img src="img/CASCADE.png" width="500">
</div>



**SET NULL**

SET NULL: Establece a NULL el valor de la clave secundaria cuando se elimina el registro en la tabla principal o se modifica el valor del campo referenciado.

Lo que vemos en la imagen es el resultado de esta consulta:

DELETE FROM ciudades WHERE ciudad_id=1;

Aquí la columna ciudad_id de la tabla personas establecerá los valores a NULL, en todas las filas cuyo ciudad_id sea igual a 1. Y como es evidente, se borrará en ciudades la ciudad con id 1


<div align="center">
  <img src="img/SETNULL.png" width="500">
</div>
Por lo que ocurre en este caso es importante señalar que este CREATE TABLE:

```sql
CREATE TABLE IF NOT EXISTS personas
(
    persona_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    persona_nom VARCHAR(70),
    ciudad_id INT NOT NULL,
    FOREIGN KEY fk_ciudad(ciudad_id) REFERENCES ciudades(ciudad_id)
    ON DELETE SET NULL

)ENGINE=INNODB;
```
Arrojará el error:

> Cannot add foreign key constraint

Creo que no es difícil averiguar el por qué :)

**NO ACTION**

NO ACTION: En MySQL funciona igual que RESTRICT. Ver explicación más abajo.

## <a name="Creando-el-blog-de-las-tablas-dependientes">Creando el blog de las tablas dependientes</a>

El comando “cascade” sirve para que cada que se haga un update en la tabla principal, se refleje también en la tabla en la que estamos creando la relación.

```sql
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(130) NOT NULL,
  `fecha_publicacion` timestamp NULL DEFAULT NULL,
  `contenido` text NOT NULL,
  `estatus` char(8) DEFAULT 'activo',
  `usuario_id` int(11) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_usuarios_idx` (`usuario_id`),
  KEY `posts_categorias_idx` (`categoria_id`),
  CONSTRAINT `posts_categorias` FOREIGN KEY (`categoria_id`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `posts_usuarios` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

CREATE TABLE `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cuerpo_comentario` text NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `comentarios_usuario_idx` (`usuario_id`),
  KEY `comentarios_post_idx` (`post_id`),
  CONSTRAINT `comentarios_post` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `comentarios_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

## <a name="Creando-el-blog-de-tablas-transitivas">Creando el blog de tablas transitivas</a>

- Las tablas transitivas sirven como puente para unir dos tablas. No tienen contenido semántico.

```sql
CREATE TABLE `posts_etiquetas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `etiqueta_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postsetiquetas_post_idx` (`post_id`),
  KEY `postsetiquetas_etiquetas_idx` (`etiqueta_id`),
  CONSTRAINT `postsetiquetas_etiquetas` FOREIGN KEY (`etiqueta_id`) REFERENCES `etiquetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `postsetiquetas_post` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;
```

**Tip herramienta para profecionales**

> Dbemos ir al meno  de Workbench y selecionamos Database y luego Reverse Engineer nos reproduce el esquema del cual nos basamos para crear nuestras tablas. Es útil cuando llegas a un nuevo trabajo y quieres entender cuál fue la mentalidad que tuvieron al momento de crear las bases de datos.


# <a name="Consultas-a-una-base-de-datos">Consultas a una base de datos</a>

## <a name="¿Por-qué-las-consultas-son-tan-importantes?">¿Por qué las consultas son tan importantes?</a>

Las consultas o queries a una base de datos son una parte fundamental ya que esto podría salvar un negocio o empresa.
Alrededor de las consultas a las bases de datos se han creado varias especialidades como ETL o transformación de datos, business intelligence e incluso machine learning.

ETL La palabra ETL correspondería al acrónimo de:
- Extract (Extraer)
- Transform (Transformar)
- Load (Cargar)

ETL hace parte del proceso de integración de datos, mas aun es un componente muy importante que completa el resultado final en la relación de aplicaciones y sistemas.

## <a name="Estructura-básica-de-un-Query">Estructura básica de un Query</a>

Los queries son la forma en la que estructuramos las preguntas que se harán a la base de datos. Transforma preguntas en sintaxis.

El query tiene básicamente 2 partes: SELECT y FROM y puede aparecer una tercera como WHERE.

**Un Query simplre de Consultar toda la tabla**
```sql
-- ##################################################################### --

-- SELECT == Selecionar
-- FROM == De donde
-- WHERE == Condicion

-- ##################################################################### --

-- Filtramos toda la tabla
-- La estrellita o asterisco * quiere decir que vamos a
-- seleccionar todo sin filtrar campos.
SELECT * FROM nombreDeLaTabla;
-- Ejemplo:
SELECT * FROM cliente;

-- ##################################################################### --

-- Filtramos datos de una tabla con una condicion
-- solo si se cumple la condicion los mostrara
SELECT * FROM nombreDeLaTabla WHERE atributo = 'valor';
-- Ejemplo:
SELECT * FROM cliente WHERE edad > 18;

-- ##################################################################### --
```

## <a name="SELECT">SELECT</a>

SELECT se encarga de proyectar o mostrar datos.

```sql
-- ##################################################################### --

-- Consulta toda la tabla
SELECT * FROM nombreDeLaTabla;
-- Ejemplo:
SELECT * FROM cliente;

-- ##################################################################### --

-- Consultar solo algunos  atributos de una tabla
SELECT atributo, atributo1 FROM nombreDeLaTabla;
-- Ejemplo:
SELECT id, nombre FROM cliente;

-- ##################################################################### --
```

El nombre de las columnas o campos que estamos consultando puede ser cambiado utilizando AS luego colocando el nuevo nombre a la columna o campo.

```sql
-- AS o Alias
SELECT atributoViejoNombre AS atributoNuevoNombre FROM nombreDeLaTabla;
-- Ejemplo:
SELECT id AS identidad FROM cliente;
```
Existe una función de SELECT para poder contar la cantidad de registros. Esa información (un número) será el resultado del query:

```sql
-- COUNT() o Contar
SELECT COUNT(*) FROM nombreDeLaTabla;
-- Ejemplo:
SELECT COUNT(*) FROM cliente;
```

## <a name="FROM">FROM</a>

FROM indica de dónde se deben traer los datos y puede ayudar a hacer sentencias y filtros complejos cuando se quieren unir tablas. La sentencia compañera que nos ayuda con este proceso es JOIN.

Los diagramas de Venn son círculos que se tocan en algún punto para ver dónde está la intersección de conjuntos. Ayudan mucho para poder formular la sentencia JOIN de la manera adecuada dependiendo del query que se quiere hacer.

<div align="center">
  <img src="img/conjuntos.png" width="1000">
</div>
<div align="center">
  <img src="img/JOIN.jpeg" width="1000">
</div>

## <a name="Utilizando-la-sentencia-FROM">Utilizando la sentencia FROM</a>

**Diferencia**

```sql
-- ##################################################################### --

-- LEFT JOIN: Trae todo los datos de la tabla A, que esten o no esten de la tabla B.
-- Uniremos dos tablas uno del lado izquierdo y el otro del lado derecho
-- En estos casos se podran unir grasias a su llave primaria y a su llave foranea
SELECT * FROM tabla LEFT JOIN tabla1 ON
tabla.atributo(llavePrimaria) = tabla1.atributo1(llaveForanea);
-- Ejemplo:
SELECT * FROM usuario LEFT JOIN post ON usuario.id = post.usuario_id;

-- ##################################################################### --

-- Trae todo los datos de la tabla A, que no esten en la tabla B.
SELECT * FROM tabla LEFT JOIN tabla1 ON
tabla.atributo(llavePrimaria) = tabla1.atributo1(llaveForanea)
WHERE tabla1.atributo1(llaveForanea) IS NULL;
-- Ejemplo:
SELECT * FROM usuario LEFT JOIN post
ON usuario.id = post.usuario_id WHERE post.usuario_id IS NULL;

-- ##################################################################### --
```


```sql
-- ##################################################################### --

-- RIGHT JOIN: Trae todo los datos de la tabla B, que esten o no esten de la tabla A.
-- Uniremos dos tablas uno del lado izquierdo y el otro del lado derecho
-- En estos casos se podran unir grasias a su llave primaria y a su llave foranea
SELECT * FROM tabla RIGHT JOIN tabla ON
tabla.atributo(llavePrimaria) = tabla1.atributo1(llaveForanea);
-- Ejemplo:
SELECT * FROM usuario RIGHT JOIN post ON usuario.id = post.usuario_id;

-- ##################################################################### --

-- Trae todo los datos de la tabla B, que no esten en la tabla A.
SELECT * FROM tabla RIGHT JOIN tabla1 ON
tabla.atributo(llavePrimaria) = tabla1.atributo1(llaveForanea)
WHERE tabla1.atributo1(llaveForanea) IS NULL;
-- Ejemplo:
SELECT * FROM usuario RIGHT JOIN post
ON usuario.id = post.usuario_id WHERE post.usuario_id IS NULL;
-- ##################################################################### --
```

**Interseccion**

```sql
-- INNER JOIN: Solo arrastra valores que esten tanto en la tabla A como en la B
-- Uniremos dos tablas uno del lado izquierdo y el otro del lado derecho
-- En estos casos se podran unir grasias a su llave primaria y a su llave foranea
SELECT * FROM tabla INNER JOIN tabla1 ON
tabla.atributo(llavePrimaria) = tabla1.atributo1(llaveForanea);
-- Ejemplo:
SELECT * FROM usuario INNER JOIN post ON usuario.id = post.usuario_id;
```

Ejemplos:

> Estos ejemplos no tienen espliacion porque en este punto ya deberia saber leer codio SQL

```sql
-- ##################################################################### --
-- Ejemplo 1
select CONCAT(empleado_nombre1, ' ', empleado_apellido1) AS nombre_empleado,
usuario, agencia_nombre as agencia  
from empleado as a
inner join usuario as b on a.empleado_id = b.empleado_id
inner join agencia as c on b.agencia_id = c.agencia_id;

-- ##################################################################### --

-- Ejemplo 2
SELECT socio_nombre1, socio_apellido1, socio_dpi, genero_nombre, pais_nombre,
nacionalidad_nombre, profesion_nombre, a.usuario_registro
FROM socio AS a
INNER JOIN genero AS b ON a.genero_id = b.genero_id
INNER JOIN nacionalidad AS c ON a.nacionalidad_id = c.nacionalidad_id
INNER JOIN profesion AS d ON a.profesion_id = d.profesion_id
INNER JOIN pais AS e ON a.pais_id = e.pais_id;

-- ERROR 1052 (23000): Column 'usuario_registro' in field list is ambiguous
-- Lo que trata de decir este error esque estamos tratando de traer un atributo
-- que tambien existen en alguna otra tabla y se genera el conflicto no sabe
-- de que tabla sacar ese atributo, y nos manda ese error, por eso debemos
-- Idicarle directamente de que tabla lo sacara
-- El atributo usuario_registro se repite en algun otra tabla por eso es necesario
-- colocar a.usuario_registro
-- ##################################################################### --
```


**Union**

```sql
-- UNION: Trae todo de la tabla A y B.
-- Uniremos dos tablas uno del lado izquierdo y el otro del lado derecho
-- En estos casos se podran unir grasias a su llave primaria y a su llave foranea
-- FULL OUTER no es estandar en casi todo los manejadores y no funciona en MYSQL
-- Para poder lograr una UNION en MYSQl debemos de hacer un script compuesto
SELECT * FROM tabla LEFT JOIN tabla1 ON
tabla.atributo(llavePrimaria) = tabla1.atributo1(llaveForanea)
UNION
SELECT * FROM tabla RIGHT JOIN tabla1 ON
tabla.atributo(llave primaria) = tabla1.atributo1(llaveForanea);
-- Ejemplo:
SELECT * FROM usuario LEFT JOIN post ON usuario.id = post.usuario_id
UNION
SELECT * FROM usuario RIGHT JOIN post ON usuario.id = post.usuario_id;
```

**Diferencia Simetrica**

```sql
-- ##################################################################### --

-- OUTER JOIN: Trae todo de la tabla A y B; pero solo informacion que no guarde relacion
-- una tabla con la otra
-- Uniremos dos tablas uno del lado izquierdo y el otro del lado derecho
-- En estos casos se podran unir grasias a su llave primaria y a su llave foranea
SELECT * FROM tabla LEFT JOIN tabla1 ON
tabla.atributo(llave primaria) = tabla1.atributo1(llaveForanea)
WHERE tabla1.atributo1(llaveForanea) IS NULL
UNION
SELECT * FROM tabla RIGHT JOIN tabla1 ON
tabla.atributo(llave primaria) = tabla1.atributo1(llaveForanea)
WHERE tabla1.atributo1(llaveForanea) IS NULL;
-- Ejemplo:
SELECT * FROM usuario LEFT JOIN post ON usuario.id = post.usuario_id
WHERE post.usuario_id IS NULL
UNION
SELECT * FROM usuario RIGHT JOIN post ON usuario.id = post.usuario_id
WHERE post.usuario_id IS NULL;
-- ##################################################################### --
```

## <a name="WHERE">WHERE</a>

WHERE es la sentencia que nos ayuda a filtrar tuplas o registros dependiendo de las características que elegimos.

- La propiedad LIKE nos ayuda a traer registros de los cuales conocemos sólo una parte de la información.
- La propiedad BETWEEN nos sirve para arrojar registros que estén en el medio de dos. Por ejemplo los registros con id entre 20 y 30.



```sql
-- ##################################################################### --

-- WHERE:
-- Filtramos datos de una tabla con una condicion
-- solo si se cumple la condicion los mostrara

-- ##################################################################### --

SELECT * FROM tabla WHERE atributo = 'valor';
-- Ejemplo: Consultamos todos los registros con el id 1 hasta el 100
SELECT * FROM cliente WHERE id <= 100;
-- Ejemplo: Consultamos todos los registros con el id 51 para arriba
SELECT * FROM cliente WHERE id > 50;

-- ##################################################################### --

SELECT * FROM tabla WHERE atributo = 'valor';
-- Ejemplo: Consultamos todos los registros de la tabla con el estado activo
SELECT * FROM cliente WHERE estado = 'activo';

-- ##################################################################### --

SELECT * FROM tabla WHERE atributo != 'valor';
-- Ejemplo: Consultamos todos los registros de la tabla con el estado
-- inactivo(Osea que sea diferente que activo)
SELECT * FROM cliente WHERE estado != 'activo';

-- ##################################################################### --

SELECT * FROM tabla WHERE atributo LIKE '%palabraClave%';
-- Ejemplo: Consultamos todos los registros de la tabla que contengan per
-- como valor del atributo apellido
SELECT * FROM cliente WHERE apellido LIKE '%Per%';

-- ##################################################################### --

SELECT * FROM tabla WHERE atributo BETWEEN 'valor' AND 'Valor1';
-- Ejemplo: Consultamos todos los registros de la tabla que estes en el
-- rango de  5 hasta el 80 del atributo edad
SELECT * FROM cliente WHERE edad BETWEEN 5 AND 80;

-- ##################################################################### --

SELECT * FROM tabla WHERE atributo BETWEEN 'valor' AND 'Valor1';
-- Ejemplo: Consultamos todos los registros de la tabla que estes en el
-- rango de de 2000-01-01 hasta el 2020-01-01 del atributo fecha
SELECT * FROM cliente WHERE fecha BETWEEN '2000-01-01' AND '2020-01-01';

-- ##################################################################### --

-- YEAR(): año
SELECT * FROM tabla WHERE YEAR(atributo) BETWEEN 'valor' AND 'Valor1';
-- Ejemplo: Consultamos todos los registros de la tabla que estes en el
-- rango de de 2000 hasta el 2020 del atributo fecha
SELECT * FROM cliente WHERE YEAR(fecha) BETWEEN '2000' AND '2020';

-- ##################################################################### --

-- MONTH(): mes
SELECT * FROM tabla WHERE MONTH(atributo) BETWEEN 'valor' AND 'Valor1';
-- Ejemplo: Consultamos todos los registros de la tabla que estes en el
-- rango de de 01 hasta el 01 del atributo fecha
SELECT * FROM cliente WHERE MONTH(fecha) BETWEEN '01' AND '04';

-- ##################################################################### --
```

```sh
NOT negacion
Los operadores LIKE y BETWEEN AND, pueden ser negados
NOT LIKE
NOT BETWEEEN – AND –
```

## <a name="Utilizando-la-sentencia-WHERE-nulo-y-no-nulo">Utilizando la sentencia WHERE nulo y no nulo</a>


El valor nulo en una tabla generalmente es su valor por defecto cuando nadie le asignó algo diferente. La sintaxis para hacer búsquedas de datos nulos es IS NULL. La sintaxis para buscar datos que no son nulos es IS NOT NULL


```sql
-- ##################################################################### --

-- IS NULL: búsquedas de datos nulos
SELECT * FROM nombreDeLaTabla WHERE atributo IS NULL;
-- Ejemplo: Trae todos los registros donde el atributo id del clinte sea
-- nulo(O donde no ahi registros)
SELECT * FROM cliente WHERE id IS NULL;

-- ##################################################################### --

-- IS NOT NULL: buscar datos que no son nulos
SELECT * FROM nombreDeLaTabla WHERE atributo IS NOT NULL;
-- Ejemplo: Trae todos los registros donde el atributo id del clinte no
-- sea nulo(o que tenga registros)
SELECT * FROM cliente WHERE id IS NOT NULL;

-- ##################################################################### --

-- AND: Es el “y” lógico. Evalúa dos condiciones y devuelve un valor de
-- verdad sólo si ambas son ciertas.
SELECT * FROM nombreDeLaTabla WHERE atributo IS NOT NULL AND atributo1 = 'valor1';
-- Ejemplo: Filtramos todos los registros donde el atributo id del
-- clinte no sea nulo(o que tenga registros) y que el atributo activo sea inactivo
-- (Trae todos los registro si cumplen las dos condiciones)
SELECT * FROM cliente WHERE id IS NOT NULL AND activo = 'inactivo';

-- ##################################################################### --

-- OR: 	Es el “o” lógico. Evalúa dos condiciones y devuelve un valor de
-- verdad si alguno de las dos es cierta.
SELECT * FROM nombreDeLaTabla WHERE atributo IS NOT NULL OR atributo1 = 'valor1';
-- Ejemplo: Filtramos todos los registros donde el atributo id del clinte no
-- sea nulo(o que tenga registros) o que el atributo activo sea inactivo
-- (Basta con que una condición se cumpla)
SELECT * FROM cliente WHERE id IS NOT NULL OR activo = 'inactivo';

-- ##################################################################### --

-- IN: anidado.
-- El operador para comparar multiples valores en una misma columna (OR anidado)
SELECT * FROM nombreDeLaTabla WHERE atributo IN ('valor', 'valor1', 'valor2');
-- Ejemplo: Trae todos los registros donde el atributo nombre
-- del clinte sea Luis, Angel o Sulma
SELECT * FROM cliente WHERE nombre IN ('Luis', 'Angel', 'Sulma');

-- ##################################################################### --
```

**Notas**

Si un query contiene muchas condiciones encadenadas por diferentes operadores lógicos, es importante establecer la prioridad de su evaluación mediante el uso de paréntesis

```sql
SELECT * FROM posts WHERE  titulo like "%se%" AND (id < 60 OR estatus = 'inactivo');
```

Es diferente que
Como OR se evalúa después de AND con el uso de paréntesis indicamos que se evalúe antes.

```sql
SELECT * FROM posts WHERE titulo like "%se%" AND id < 60 OR estatus = 'inactivo';
```

## <a name="GROUP-BY">GROUP BY</a>

GROUP BY tiene que ver con agrupación. Indica a la base de datos qué criterios debe tener en cuenta para agrupar.


```sql
-- ##################################################################### --

-- GROUP BY: agrupación
SELECT atributo, COUNT(*) AS nombreNuevo FROM nombreDeLaTabla GROUP BY atributo;
-- Ejemplo; De agrupacion por estado activo o inactivo
-- Agrupamos el estado y lo que veremos es el estado y cuantos clietes pertenecen
-- a activo o inactivo
SELECT estado, COUNT(*) AS cantidad FROM cliente GROUP BY estado;

-- ##################################################################### --

-- GROUP BY: agrupación
SELECT YEAR(atributo) AS nombreNuevo, COUNT(*) AS nueboNombre1
FROM nombreDeLaTabla GROUP BY nombreNuevo;
-- Ejemplo; De agrupacion por fecha
-- Agrupamos la fecha_publicacion y lo que veremos es el año y
-- cuantos posts en este año fuenron escritos
SELECT YEAR(fecha_publicacion) AS año, COUNT(*) AS cantidad FROM post GROUP BY año;

-- ##################################################################### --

-- GROUP BY: agrupación de dos criterios
SELECT atributo AS nuevoNombre, MONTH(atributo1) AS nombreNuevo1, COUNT(*) AS nueboNombre2
FROM nombreDeLaTabla GROUP BY nuevoNombre, nombreNuevo1;
-- Ejemplo; Agruparemos el estado y el mes y nos regresa cunatos activos en
-- enero y cuantos inactivos en enero
SELECT estado AS estados, MONTH(fecha_publicacion) AS mes, COUNT(*) AS cantidad
FROM post GROUP BY estados, mes;

-- ##################################################################### --
```

```sh
Aparte de la función COUNT, podemos encontrar las siguientes funciones de agregado:
AVG Calcula el promedio
COUNT Cuenta los registros de un campo
SUM Suma los valores de un campo
MAX Devuelve el maximo de un campo
MIN Devuelve el mínimo de un campo
```

## <a name="ORDER-BY-y-HAVING">ORDER BY y HAVING</a>

La sentencia ORDER BY tiene que ver con el ordenamiento de los datos dependiendo de los criterios que quieras usar.

- ASC sirve para ordenar de forma ascendente.
- DESC sirve para ordenar de forma descendente.
- LIMIT se usa para limitar la cantidad de resultados que arroja el query.
- HAVING tiene una similitud muy grande con WHERE, sin embargo el uso de ellos depende del orden. Cuando se quiere seleccionar tuplas agrupadas únicamente se puede hacer con HAVING.

```sql
-- ##################################################################### --

-- ORDER BY: ordenamiento
SELECT * FROM nombreDeLaTabla ORDER BY atributo;
-- Ejemplo: Le diremos que lo ordenara de forma ascendente por el atributo edad
SELECT * FROM estudiante ORDER BY edad;

-- ##################################################################### --

-- ORDER BY ASC: ordenamiento explicito de forma ascendente
SELECT * FROM nombreDeLaTabla ORDER BY atributo ASC;
-- Ejemplo: Le diremos que lo ordenara de forma ascendente por el atributo edad
SELECT * FROM estudiante ORDER BY edad ASC;

-- ##################################################################### --

-- ORDER BY DESC: ordenamiento descendente
SELECT * FROM nombreDeLaTabla ORDER BY atributo DESC;
-- Ejemplo: Le diremos que lo ordenara de forma descendente por el atributo edad
SELECT * FROM estudiante ORDER BY edad DESC;

-- ##################################################################### --
```

```sql
-- LIMIT: Limita la consulta
SELECT * FROM nombreDeLaTabla LIMIT valor;
-- Ejemplo: Le diremos que solo traiga 5 registros
SELECT * FROM estudiante LIMIT 5;
```


```sql
-- ##################################################################### --

-- HAVING: tiene una similitud muy grande con WHERE, sin embargo el uso
-- de ellos depende del orden.
SELECT atributo atributo1, atributo2
FROM tabla GROUP BY atributo, atributo1 HAVING atributo3 > valor ORDER BY atributo;
-- Ejemplo:
-- Agrupar por mes y estatus. Contar los posts y mostrar los que se han mayores a uno.
-- Finalmente se debe ornear por mes.
SELECT MONTH(fecha_publicacion) AS Mes, status, Count(*) AS Cant
FROM posts GROUP BY Mes, status HAVING Cant > 1 ORDER BY Mes;

-- ##################################################################### --

-- En este Script existe un error lo correcto es el scrit de arriba
-- Error por usar where en una consulta agrupados
-- WHERE seleciona a selecionar los atributos pero los seleciona antes
-- de ser la parte de agrupacion por eso WEHRE no sabe el valor de Cant
-- Que fue asignodo por un alias
-- HAVING: para selección los atributos agrupados después de un GROUP BY
-- para que no nos tire error al crear agrupar siempre deberiamos de
-- utilizar HAVING

Select MONTH(fecha_publicacion) As Mes, status, Count(*) As Cant
From posts
WHERE Cant > 1
Group By Mes, status
Order By Mes;

-- ##################################################################### --
```

## <a name="El-interminable-agujero-de-conejo-(Nested-queries)">El interminable agujero de conejo (Nested queries)</a>

Los Nested queries significan que dentro de un query podemos hacer otro query. Esto sirve para hacer join de tablas, estando una en memoria. También teniendo un query como condicional del otro.

Este proceso puede ser tan profundo como quieras, teniendo infinitos queries anidados.
Se le conoce como un producto cartesiano ya que se multiplican todos los registros de una tabla con todos los del nuevo query. Esto provoca que el query sea difícil de procesar por lo pesado que puede resultar.

- Las consultas anidadas son la mejor opción cuando los valores dependen de otras tablas, y estas no se encuentran relacionadas entre si.
- Las consultas anidadas son la mejor opción para casos de INSERT, DELETE, UPDATE, cuya condición dependa del esenario explicado en el punto anterior

**Query aninado**

```sql
Select NuevaTabla.Fecha, Count(*) As Cant
From (
	Select Date(Min(fecha_publicacion)) As Fecha, Year(fecha_publicacion) As Año
	From posts Group By Año
) As NuevaTabla
Group By NuevaTabla.Fecha
Order By NuevaTabla.Fecha;

```

```sql
-- Mostrara solo el registro que tenga la fecha máxima o las más actual.
Select * From posts
Where fecha_publicacion = (
	Select Max(fecha_publicacion)
    From posts
);
```


## <a name="¿Cómo-convertir-una-pregunta-en-un-query-SQL?">¿Cómo convertir una pregunta en un query SQL?</a>

De pregunta a Query

- SELECT: Lo que quieres mostrar
- FROM: De dónde voy a tomar los datos
- WHERE: Los filtros de los datos que quieres mostrar
- GROUP BY: Los rubros por los que me interesa agrupar la información
- ORDER BY: El orden en que quiero presentar mi información
- HAVING: Los filtros que quiero que mis datos agrupados tengan
- LIMIT: La cantidad de registros que quiero

## <a name="Preguntándole-a-la-base-de-datos">Preguntándole a la base de datos</a>

GROUP_CONCAT toma el resultado del query y lo pone como campo separado por comas.

**Preguntas**

¿Cuantas etiquetas están ligadas a los blogpost?

```sql
SELECT posts.titulo, COUNT(*) num_etiquetas
FROM posts
INNER JOIN posts_etiquetas ON posts.id = posts_etiquetas.post_id
INNER JOIN etiquetas ON etiquetas.id = posts_etiquetas.etiqueta_id
GROUP BY posts.id
ORDER BY num_etiquetas DESC;
```

Saber cuales son las Etiquetas que estan ligadas a los post



```sql
SELECT posts.titulo, GROUP_CONCAT(nombre_etiqueta)
FROM posts
INNER JOIN posts_etiquetas ON posts.id = posts_etiquetas.post_id
INNER JOIN etiquetas ON etiquetas.id = posts_etiquetas.etiqueta_id
GROUP BY posts.id;
```

Saber cual son las etiquetas que no están relacionadas con un post

```sql
SELECT *
FROM etiquetas
LEFT JOIN posts_etiquetas ON etiquetas.id = posts_etiquetas.etiqueta_id
WHERE posts_etiquetas.etiqueta_id IS NULL;
```

# <a name="Introducción-a-la-bases-de-datos-NO-relacionales">Introducción a la bases de datos NO relacionales</a>

## <a name="¿Qué-son-y-cuáles-son-los-tipos-de-bases-de-datos-no-relacionales?">¿Qué son y cuáles son los tipos de bases de datos no relacionales?</a>

Respecto a las bases de datos no relacionales, no existe un solo tipo aunque se engloben en una sola categoría.

**Tipos de bases de datos no relacionales:**

- Clave - valor: Son ideales para almacenar y extraer datos con una clave única. Manejan los diccionarios de manera excepcional. Ejemplos: DynamoDB, Cassandra.
- Basadas en documentos: Son una implementación de clave valor que varía en la forma semiestructurada en que se trata la información. Ideal para almacenar datos JSON y XML. Ejemplos: MongoDB, Firestore.
- Basadas en grafos: Basadas en teoría de grafos, sirven para entidades que se encuentran interconectadas por múltiples relaciones. Ideales para almacenar relaciones complejas. Ejemplos: neo4j, TITAN.
- En memoria: Pueden ser de estructura variada, pero su ventaja radica en la velocidad, ya que al vivir en memoria la extracción de datos es casi inmediata. Ejemplos: Memcached, Redis.
- Optimizadas para búsquedas: Pueden ser de diversas estructuras, su ventaja radica en que se pueden hacer queries y búsquedas complejas de manera sencilla. Ejemplos: BigQuery, Elasticsearch.



# <a name="Servicios-administrados-y-jerarquía-de-datos">Servicios administrados y jerarquía de datos</a>

Firebase (Firestore). Es una plataforma muy utilizada para el desarrollo de aplicaciones web y aplicaciones móviles. Como usa un conjunto de herramientas multiplataforma es compatible con grandes plataformas, como IOS, Android, aplicaciones web, Unity y C++. Es muy recomendable para desarrollos.

**Jerarquía de datos**

- Base de Datos: Contiene toda la información que se quiere guardar.
- Colección: Es igual a las tablas en las bases de datos relacionales. Son objetos que agrupan (Documentos) la información que se desea guardar.
- Documento: Es la información que se quiere guardar. Se guarda en un formato muy parecido al formato JSON (es un lenguaje que se utiliza para comunicarse con diferentes lenguajes o aplicaciones). Los documentos dentro de ellos contienen datos.


**Esta es la estructura de una base de datos No relacional basada en documentos**

<div align="center">
  <img src="img/estructura.png" width="500">
</div>














- [Introducción](#Introducción)
# <a name="Introducción">Introducción</a>


```sh
$ sudo yum install python
```
